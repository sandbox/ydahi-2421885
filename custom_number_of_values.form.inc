<?php
/**
 * @file
 * Custom Number of Values hook to set global range for number of values.
 */

/**
 * Using hook_form_alter to change the field cardinality.
 */
function custom_number_of_values_form_alter(&$form, &$form_state, $form_id) {

  // Increase the max cardinality to 20.
  $lowerlimit = variable_get('custom_number_of_values_minimum', '1');
  $upperlimit = variable_get('custom_number_of_values_maximum', '20');
  $range = drupal_map_assoc(range($lowerlimit, $upperlimit));
  $new_options = array(FIELD_CARDINALITY_UNLIMITED => t('Unlimited')) + $range;
  // Change the options.
  $form['field']['cardinality']['#options'] = $new_options;
}

