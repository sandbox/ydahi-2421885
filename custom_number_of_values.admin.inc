<?php
/**
 * @file
 * Custom Number of Values administration forms.
 */

/**
 * Custom Number of Values administration page.
 */
function custom_number_of_values_admin_form($form, &$form_state) {
  $form['custom_range'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Range'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['custom_range']['custom_number_of_values_minimum'] = array(
    '#type' => 'textfield',
    '#title' => t('Lower limit for number of values'),
    '#description' => t("The lower limit of the range available for multi-value fields. Default is 1."),
    '#default_value' => variable_get('custom_number_of_values_minimum', 1),
    '#required' => TRUE,
    '#size' => 5,
  );
  $form['custom_range']['custom_number_of_values_maximum'] = array(
    '#type' => 'textfield',
    '#title' => t('Upper limit for number of values'),
    '#description' => t("The upper limit of the range available for multi-value fields. Default is 20."),
    '#default_value' => variable_get('custom_number_of_values_maximum', 20),
    '#required' => TRUE,
    '#size' => 5,
  );
  return system_settings_form($form);
}

/**
 * Validate the admin form.
 */
function custom_number_of_values_admin_form_validate($form, &$form_state) {
  // Make sure the values provided are positive integers.
  $lowerlimit = $form_state['values']['custom_number_of_values_minimum'];
  $upperlimit = $form_state['values']['custom_number_of_values_maximum'];
  if ((is_numeric($lowerlimit) && $lowerlimit > 0 && (is_numeric($upperlimit) && $upperlimit > 0))) {
    if ($upperlimit > $lowerlimit) {
      // Good to go.
    }
    else {
      form_set_error('custom_number_of_values_limit', t("The value for the Upper limit must be greater
	  than the value of the Lower limit."));
    }
  }
  else {
    form_set_error('custom_number_of_values_limit', t("The values of the limits must be integers greater than 0."));
  }
}

