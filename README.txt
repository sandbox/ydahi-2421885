Custom Number of Values README.txt
==================================

INTRODUCTION
------------
The problem this module tries to address is the limiting defaults
range of the "Number of values" available for multi-value fields.

This module makes it possible for users with the "Administer content 
types" permission to set a custom range for the number of values by
setting a lower and upper value.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/ydahi/2421885
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2421885


INSTALLATION
------------

1. Install the custom_number_of_values module directory in the directory
	where you keep contributed modules (e.g. sites/all/modules/).
	
2. Go to the Modules page
   - Enable the Custom number of values module.
   Click on Save configuration.

3. Configure custom ranges
   - Go to Configuration -> Content Authering -> Custom number of values
   - To alter the lower limit, change the field:
       Lower limit for number of values
   - To alter the upper limit, change the field:
       Upper limit for number of values
   Click on Save configuration.
   
3. Set a range for each multi-value field
   - Go to the edit form of any CCK field that has multiple values
   - From the drop down labeled "Number of values"
   		Select a value from the range you configured in the module settings